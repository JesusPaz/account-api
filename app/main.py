"""Main file"""
import os
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from models import Client


@app.route("/")
def index():
    """Index"""
    return "This is the app index"


@app.route("/add")
def add_client():
    """Add client and money"""
    name = request.args.get('name')
    money = request.args.get('money')
    try:
        client = Client(
            name=name,
            money=money
        )
        db.session.add(client)
        db.session.commit()
        return "Client added with id={}".format(client.id)
    except Exception as expt:
        return str(expt)


@app.route("/getall")
def get_all():
    """Get all clients."""
    try:
        clients = Client.query.all()
        return jsonify([e.serialize() for e in clients])
    except Exception as expt:
        return str(expt)


@app.route("/get/<id_>")
def get_by_id(id_):
    """Get client by ID."""
    try:
        client = Client.query.filter_by(id=id_).first()
        return jsonify(client.serialize())
    except Exception as expt:
        return str(expt)


@app.route("/getn/<name_>")
def get_by_name(name_):
    """Get client by Name."""
    try:
        client = Client.query.filter_by(name=name_).first()
        return jsonify(client.serialize())
    except Exception as expt:
        return str(expt)


if __name__ == '__main__':
    app.run(debug=True)
