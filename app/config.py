"""Flask Configuration"""
from os import environ, path
basedir = path.abspath(path.dirname(__file__))


class Config(object):
    """Default config"""
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URI')

class ProdConfig(Config):
    """Production Config"""
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False


class DevConfig(Config):
    """Development Config"""
    FLASK_ENV = 'development'
    DEBUG = True
    TESTING = True
