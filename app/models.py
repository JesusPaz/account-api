"""Models file"""
from main import db


class Client(db.Model):
    """Client model"""
    __tablename__ = 'clients'

    # Fields
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    money = db.Column(db.Integer)

    # Initialize database
    def __init__(self, name, money):
        """Init method"""
        self.name = name
        self.money = money

    def __repr__(self):
        """repr method"""
        return '<id {}>'.format(self.id)

    # For JSON serialization purposes
    def serialize(self):
        """Serialize method"""
        return {
            'id': self.id,
            'name': self.name,
            'money': self.money
        }
