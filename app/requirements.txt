Flask==2.0.0
Flask-Migrate==2.7.0
Flask-Script==2.0.6
Flask-SQLAlchemy==2.5.1
PyMySQL==1.0.2
SQLAlchemy==1.4.15
pylint==2.8.2
pytest==6.2.4
