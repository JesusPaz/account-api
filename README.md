# Account API Repository

Variables required to run the application:
```
export FLASK_APP=main.py
export APP_SETTINGS="config.DevConfig"
export DATABASE_URI="mysql+pymysql://root:toor@localhost:3311/db"
```
To run using Flask:
```
cd app
python3 -m flask run    
```
To run the docker compose which starts a mysql container:
```
docker-compose up
```
Build and execute docker image:
```
docker build -t flask-app .
docker run -d --name flask -p 80:80 flask-app
```
