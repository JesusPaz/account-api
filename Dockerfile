FROM tiangolo/uwsgi-nginx-flask:python3.6

ENV APP_SETTINGS="config.DevConfig"
ENV DATABASE_URI="mysql+pymysql://user:pass@localhost:3306/db"

COPY ./app /app
RUN pip install -r requirements.txt
